let clear = false;
let calc = "";

$(document).ready(function() {
  $(".button").click(function() {
    let text = $(this).attr("value");

      if(parseInt(text, 10) == text || text === "." || text === "/" || text === "*" || text === "-" || text === "+" || text === "%") {
      if(clear === false) {
        calc += text;
        $(".textbox").val(calc);
      } else {
        calc = text;
        $(".textbox").val(calc);
        clear = false;
      }
    }

     if(text === "C") {
      calc = calc.slice(0, -1);
      $(".textbox").val(calc);
    }  else if(text === "=") {
      calc = eval(calc);
      $(".textbox").val(calc);
      clear = true;
    }

     if(text === "m-" || text === "m+") {
         localStorage.setItem('SavedVal', calc);
         calc = "m                                        ";
         $(".textbox").val(calc);
     }
     if(text === "mrc") {
         calc = localStorage.getItem('SavedVal');
         $(".textbox").val(calc);
     }
  });

    $(document).keydown(function (e) {

        if (e.key === "1" || e.key === "2" || e.key === "3" || e.key === "4" || e.key === "5" || e.key === "6" || e.key === "7" || e.key === "8" || e.key === "9" || e.key === "0" || e.key === "(" || e.key === ")" || e.key === "+" || e.key === "-" || e.key === "/" || e.key === "*" || e.key === "," || e.key === "." || e.key === "%"){
            if (clear === false){
                calc += e.key;
                $(".textbox").val(calc);
            }else {
                calc = e.key;
                $(".textbox").val(calc);
                clear = false;
            }
        }

        if(e.which === 13) {
            calc = eval(calc);
            $(".textbox").val(calc);
            clear = true;
        }
        if(e.which === 8) {
            calc = calc.slice(0, -1);
            $(".textbox").val(calc);
        }
    });
});
